class Escuela implements EscuelaImpl {

    private Estudiante estudiantes[]  = {
        new Estudiante("Juan", 24),
                new Estudiante("Erika", 20),
                new Estudiante("Emilio", 23),
                new Estudiante("Karina", 21),
                new Estudiante("Eduardo", 24),
                new Estudiante("Tomas", 25)
    };

    @Override
    public int getEdadMinima() {
        int edadMinimaActual = estudiantes[0].getEdad();
        for (int i = 0; i < estudiantes.length ; i++) {
            if ( edadMinimaActual >  estudiantes[i].getEdad()){
                edadMinimaActual = estudiantes[i].getEdad();
            }
        }

        return edadMinimaActual;
    }

    @Override
    public int getEdadMaxima() {
        int edadMaximaActual = estudiantes[0].getEdad();
        for (int i = 0; i < estudiantes.length ; i++) {
            if ( edadMaximaActual <  estudiantes[i].getEdad()){
                edadMaximaActual = estudiantes[i].getEdad();
            }
        }

        return edadMaximaActual;
    }

    @Override
    public float getEdadPromedio() {
        float suma = 0;
        for ( int i = 0; i < estudiantes.length; i++){
            suma  += estudiantes[i].getEdad();
        }
        return suma / estudiantes.length;
    }

    @Override
    public String toString(){
        return  String.format("Edad Máxima: %d Edad Minima: %d Promedio: %f", getEdadMaxima(), getEdadMinima(), getEdadPromedio());
    }
}