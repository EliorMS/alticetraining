public interface EscuelaImpl {
    int getEdadMinima();
    int getEdadMaxima();
    float getEdadPromedio();
}